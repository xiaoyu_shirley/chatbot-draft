#!/usr/bin/env python
# coding: utf8
"""A simple example of extracting relations between phrases and entities using
spaCy's named entity recognizer and the dependency parse. Here, we extract
money and currency values (entities labelled as MONEY) and then check the
dependency tree to find the noun phrase they are referring to – for example:
$9.4 million --> Net income.

Compatible with: spaCy v2.0.0+
"""
from __future__ import unicode_literals, print_function

import random

import plac
from nltk.sentiment.vader import SentimentIntensityAnalyzer

import spacy

# ner_sentences = [
#     "google, headquartered in Mountain view, unveiled the new android phone at the Consumer Electronic Show.",
#     "Sundar Pichai said in his keynote that users love their new Android phones.",
#     "AllenNLP is a PyTorch-based natural language processing library developed at the Allen Institute for Artificial Intelligence in Seattle.",
#     "Did Uriah honestly think he could beat The Legend of Zelda in under three hours?",
#     "My preferred candidate is Cary Moon, but she won't be the next mayor of Seattle.",
#     "If you like Paul McCartney you should listen to the first Wings album.",
#     "When I told John that I wanted to move to Alaska, he warned me that I'd have trouble finding a Starbucks there."]
#
# sentiment_sentences = ["Can't believe the server handed me a cold coffee instead of hot - JUST GREAT!",
#                        "Wish this brand would update its packaging!",
#                        "I love the Espresso, but can't stand the Macchiato at this place.",
#                        "I hate the taste of coffee.",
#                        "This phone is much better than my old phone.",
#                        "The service was horrible but the food was absolutely brilliant.",
#                        "It's a nice weather to take a walk."]

end_sentences = ["It's a great pleasure to talk with you, see you next time!\n",
                 "See you, have a nice day!\n",
                 "OK, bye then.\n",
                 "Bye-bye.\n",
                 "Look forward to our next conversation already, good bye!\n"]

greet_sentences = ["Good day!\n",
                   "Nice to meet you!\n",
                   "How are you today?\n"]

people_sentences = ["How do you know about {}?\n",
                    "Is {} a good person?\n",
                    "I've never heard of {}.\n",
                    "It's good to know the story about {}.\n"]

norp_sentences = ["How do you know about {}?\n",
                  "Is {} a good place?\n",
                  "I've never heard of {}.\n",
                  "Have you ever been there before?\n",
                  "It's good to know your story about {}.\n",
                  "I really want to have a visit to {}.\n"]

event_sentences = ["How do you know about {}?\n",
                   "Is {} a good event?\n",
                   "I've never heard of {}.\n",
                   "Have you ever been there before?\n",
                   "It's good to know about {}.\n",
                   "I really want to have a visit to {}.\n"]

other_sentences = ["I've never heard of {}.\n",
                   "Can you tell me more about {}?\n",
                   "Do you like {}?\n",
                   "It's good to know about {}.\n"]


@plac.annotations(
    model=("Model to load (needs parser and NER)", "positional", None, str))
def main(model='en_core_web_sm'):
    nlp = spacy.load(model)
    sid = SentimentIntensityAnalyzer()
    random.shuffle(greet_sentences)
    sentence = raw_input("{}What can I do for you?\n".format(greet_sentences[0]))
    last_response = ""
    while True:
        if sentence.lower() == "end":
            random.shuffle(end_sentences)
            print(end_sentences[0])
            break

        ss = sid.polarity_scores(sentence)
        scores = [ss['neu'], ss['neg'], ss['pos']]
        max_score = max(scores)
        sentiment = None
        if max_score == scores[0]:
            sentiment = "Emm."
        elif max_score == scores[1]:
            sentiment = "Sorry to hear that."
        elif max_score == scores[2]:
            sentiment = "Good for you!"

        doc = nlp("{}".format(sentence))

        if len(doc.ents) == 0:
            sentence = raw_input("{} Is there anything else you wanna talk?\n".format(sentiment))
            continue

        longest_ent = ""
        for ent in doc.ents:
            if len(ent.string) > len(longest_ent):
                longest_ent = ent

        if longest_ent.label_ == "PERSON":
            respond_sentence = last_response
            while respond_sentence == last_response:
                random.shuffle(people_sentences)
                respond_sentence = people_sentences[0]
            last_response = respond_sentence

            if "{}" in respond_sentence:
                respond_sentence = sentiment + respond_sentence.format(longest_ent)
                sentence = raw_input(respond_sentence)
            else:
                respond_sentence = sentiment + respond_sentence
                sentence = raw_input(respond_sentence)

        elif longest_ent.label_ in ["NORP", "GPE"]:
            respond_sentence = last_response
            while respond_sentence == last_response:
                random.shuffle(norp_sentences)
                respond_sentence = norp_sentences[0]
            last_response = respond_sentence

            if "{}" in respond_sentence:
                respond_sentence = sentiment + respond_sentence.format(longest_ent)
                sentence = raw_input(respond_sentence)
            else:
                respond_sentence = sentiment + respond_sentence
                sentence = raw_input(respond_sentence)

        elif longest_ent.label_ == "EVENT":
            respond_sentence = last_response
            while respond_sentence == last_response:
                random.shuffle(event_sentences)
                respond_sentence = event_sentences[0]
            last_response = respond_sentence

            if "{}" in respond_sentence:
                respond_sentence = sentiment + respond_sentence.format(longest_ent)
                sentence = raw_input(respond_sentence)
            else:
                respond_sentence = sentiment + respond_sentence
                sentence = raw_input(respond_sentence)

        else:
            respond_sentence = last_response
            while respond_sentence == last_response:
                random.shuffle(other_sentences)
                respond_sentence = other_sentences[0]
            last_response = respond_sentence

            if "{}" in respond_sentence:
                respond_sentence = sentiment + respond_sentence.format(longest_ent)
                sentence = raw_input(respond_sentence)
            else:
                respond_sentence = sentiment + respond_sentence
                sentence = raw_input(respond_sentence)


if __name__ == '__main__':
    plac.call(main)
